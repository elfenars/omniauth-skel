class SessionsController < ApplicationController
  def create
    render text: auth_hash.inspect
  end

  private

  def auth_hash
    request.env['omniauth.auth']
  end
end