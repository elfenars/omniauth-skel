def define_omniauth_constants
  yml = YAML.load_file("./config/omniauth_keys.yml")[Rails.env]

  # Create constants for each provider. (e.g: FACEBOOK_ID and FACEBOOK_SECRET)
  # Its based on the key names of the omniauth_keys file.
  yml.keys.each do |provider|
    yml[provider].keys.each do |type|
      eval "#{provider.upcase + '_' + type.upcase} = #{'yml[provider][type]'}"
    end
  end
end

OmniAuth.config.logger = Rails.logger

Rails.application.config.middleware.use OmniAuth::Builder do
  define_omniauth_constants # Where the magic happens.

  # https://github.com/mkdynamic/omniauth-facebook
  provider :facebook, FACEBOOK_ID, FACEBOOK_SECRET

  # https://github.com/arunagw/omniauth-twitter
  provider :twitter, TWITTER_ID, TWITTER_SECRET

  # https://github.com/zquestz/omniauth-google-oauth2
  provider :google_oauth2, GOOGLE_ID, GOOGLE_SECRET
end

